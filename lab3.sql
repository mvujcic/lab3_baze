CREATE TABLE STUDENTI (
	ID NUMBER(9, 0) UNIQUE NOT NULL,
	JMBAG NUMBER(8, 0) UNIQUE NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(20) UNIQUE NOT NULL,
	password VARCHAR2(10) NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	constraint STUDENTI_PK PRIMARY KEY (ID),
	constraint ck_spol_studenti CHECK (spol IN (0, 1)));

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on STUDENTI
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE NASTAVNICI (
	ID NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(20) UNIQUE NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	constraint NASTAVNICI_PK PRIMARY KEY (ID));
	constraint ck_spol_nastavnici CHECK (spol IN (0, 1));

CREATE sequence NASTAVNICI_ID_SEQ;

CREATE trigger BI_NASTAVNICI_ID
  before insert on NASTAVNICI
  for each row
begin
  select NASTAVNICI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE PREDMETI (
	ID NUMBER(9, 0) NOT NULL,
	IDnastavnika NUMBER(9, 0) NOT NULL,
	sifra VARCHAR2(10) UNIQUE NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
	ects NUMBER(1, 0) NOT NULL,
	semestar NUMBER(1, 0) NOT NULL,
	constraint PREDMETI_PK PRIMARY KEY (ID));
	constraint ck_semestar CHECK (semestar BETWEEN 1 and 6);

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on PREDMETI
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE UPISI (
	ID NUMBER(9, 0) NOT NULL,
	IDpredmeta NUMBER(9, 0) NOT NULL,
	IDstudenta NUMBER(9, 0) NOT NULL,
	datum TIMESTAMP NOT NULL,
	ocjena NUMBER(1, 0),
	constraint UPISI_PK PRIMARY KEY (ID);
	constraint ck_ocjena CHECK (ocjena BETWEEN 1 and 5));

CREATE sequence UPISI_ID_SEQ;

CREATE trigger BI_UPISI_ID
  before insert on UPISI
  for each row
begin
  select UPISI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


ALTER TABLE PREDMETI ADD CONSTRAINT PREDMETI_fk0 FOREIGN KEY (IDnastavnika) REFERENCES NASTAVNICI(ID);

ALTER TABLE UPISI ADD CONSTRAINT UPISI_fk0 FOREIGN KEY (IDpredmeta) REFERENCES PREDMETI(ID);
ALTER TABLE UPISI ADD CONSTRAINT UPISI_fk1 FOREIGN KEY (IDstudenta) REFERENCES STUDENTI(ID);
